using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Lab1.Data;
using System;
using System.Linq;

namespace Lab1.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcTodoContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcTodoContext>>()))
            {
                // Look for any Todos.
                if (context.TodoItem.Any())
                {
                    return;   // DB has been seeded
                }

                context.TodoItem.AddRange(
                    new TodoItem
                    {
                        Name = "Read book",
                        Date = DateTime.Parse("2019-12-12"),
                        IsComplete = false
                    },

                    new TodoItem
                    {
                        Name = "Play Golf",
                        Date = DateTime.Parse("2020-8-12"),
                        IsComplete = true
                    },

                    new TodoItem
                    {
                        Name = "Go to sleep",
                        Date = DateTime.Parse("2020-2-12"),
                        IsComplete = false
                    },

                    new TodoItem
                    {
                        Name = "Watch movie",
                        Date = DateTime.Parse("2005-1-4"),
                        IsComplete = true
                    }
                );
                context.SaveChanges();
            }
        }
    }
}