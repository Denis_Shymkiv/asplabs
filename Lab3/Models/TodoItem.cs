﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lab1.Models
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
         [Display(Name = "Date"), DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public bool IsComplete { get; set; }
    }
}