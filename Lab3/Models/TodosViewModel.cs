using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Lab1.Models
{
    public class TodosViewModel
    {
        public List<TodoItem> TodoItems { get; set; }
        public SelectList States { get; set; }
        public string State { get; set; }
        public string SearchString { get; set; }
    }
}