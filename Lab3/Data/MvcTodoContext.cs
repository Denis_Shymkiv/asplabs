using Microsoft.EntityFrameworkCore;
using Lab1.Models;

namespace Lab1.Data
{
    public class MvcTodoContext : DbContext
    {
        public MvcTodoContext (DbContextOptions<MvcTodoContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItem { get; set; }
    }
}