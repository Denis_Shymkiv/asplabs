﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Lab5.Models;
using Lab5.ViewsModels;

namespace Lab5.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepositoryProduct repositoryProduct;
        private readonly IRepositorySupplier repositorySupplier;

        public HomeController(IRepositoryProduct repo, IRepositorySupplier repos) { 
            repositoryProduct = repo;
            repositorySupplier = repos;
        }

        public IActionResult Index() => View(repositoryProduct.GetAllProducts);

        [HttpGet]
        public IActionResult Create()
        {
            SupplierViewModelDropDownList model = new SupplierViewModelDropDownList();
            model.Supplier = DropDownListpopulated();
            return View(model);
        }

        private List<SelectListItem> DropDownListpopulated()
        {
            return repositorySupplier.GetSupplier.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name

            }).ToList();
        }

        [HttpPost]
        public IActionResult Create(SupplierViewModelDropDownList mo)
        {
            if(!ModelState.IsValid)
            {
                mo.Supplier = DropDownListpopulated();
                return View(mo);
            }
            Product product = new Product();
            product.Name = mo.Product.Name;
            product.Category = mo.Product.Category;
            product.Price = mo.Product.Price;
            product.SupplierId = mo.Product.SupplierId;

            repositoryProduct.Create(product);
            return RedirectToAction("index");
           
            
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Product GetProduct = repositoryProduct.GetProduct(id);

            SupplierViewModelDropDownList model = new SupplierViewModelDropDownList
            {
                Product = GetProduct,
                Supplier = DropDownListpopulated()

            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(SupplierViewModelDropDownList mo)
        {
            if( !ModelState.IsValid)
            {
                mo.Supplier = DropDownListpopulated();
                return View(mo);
            }
            Product product = repositoryProduct.GetProduct(mo.Product.Id);
            product.Name = mo.Product.Name;
            product.Category = mo.Product.Category;
            product.Price = mo.Product.Price;
            product.SupplierId = mo.Product.SupplierId;

            repositoryProduct.Update(product);
            return RedirectToAction("index");

        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            repositoryProduct.Delete(id);
            return RedirectToAction("index");
        }

    }
}