﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab5.Models;
using Lab5.ViewsModels;

namespace Lab5.Controllers
{
    public class SupplierController : Controller
    {
        private readonly IRepositorySupplier repositorySupplier;
        private readonly IRepositoryProduct repositoryProduct;
        public SupplierController(IRepositorySupplier supplier, IRepositoryProduct repos)
        {
            repositorySupplier = supplier;
            repositoryProduct = repos;
        }

        public IActionResult Index()
        {
        
            return View(repositorySupplier.GetSupplier);
        }


        public IActionResult ViewProducts(int id)
        {
            ViewBag.SuuplierName = repositoryProduct.GetNameByIdSupplier(id);

            return View(repositoryProduct.GetProductByIdSupplier(id));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Supplier supplier)
        {
            if(!ModelState.IsValid)
            {

                return View(supplier);
            }

            repositorySupplier.Create(supplier);
           return RedirectToAction("index");
        }
        public IActionResult Edit(int id)
        {
            return View(repositorySupplier.GetSupplierById(id));
        }
        [HttpPost]
        public IActionResult Edit(Supplier supplier)
        {
            if (!ModelState.IsValid)
            {

                return View(supplier);
            }
            repositorySupplier.Update(supplier);
            return RedirectToAction("index");

        }

        public IActionResult Delete(int id)
        {
            repositorySupplier.Delete(id);
            return RedirectToAction("index");
        }

    }
}