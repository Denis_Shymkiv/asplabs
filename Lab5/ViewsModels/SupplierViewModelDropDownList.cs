﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Lab5.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab5.ViewsModels
{
    public class SupplierViewModelDropDownList
    {
        public Product Product { get; set; }

        //For Select 
        [Display(Name ="Supplier Name")]
        public List<SelectListItem> Supplier { get; set; }
       
    }
}
