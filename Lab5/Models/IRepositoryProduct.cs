﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab5.Models
{
    public interface IRepositoryProduct
    {
        IEnumerable<Product> GetAllProducts { get;}
        Product GetProduct(int id);
        void Create(Product NewProduct);
        void Update(Product UpdateProduct);
        void Delete(int id);

        IEnumerable<Product> GetProductByIdSupplier(int id);
        string GetNameByIdSupplier(int id);

    }
}
