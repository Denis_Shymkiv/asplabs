﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab5.Models
{
    public class MockRepositorySupplier : IRepositorySupplier
    {
        private readonly DbContextDatabase Context;

        public MockRepositorySupplier(DbContextDatabase db)
        {
            Context = db;
        }
        public IEnumerable<Supplier> GetSupplier => Context.Suppliers.Include(p=>p.Products);

        public void Create(Supplier newSupplier)
        {
            Context.Suppliers.Add(newSupplier);
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            Context.Suppliers.Remove(new Supplier { Id = id });
            Context.SaveChanges();
           
        }

        public Supplier GetSupplierById(int id)
        {
            return Context.Suppliers.Where(x => x.Id == id).First();
        }

        public void Update(Supplier updateSupplier)
        {
            Context.Suppliers.Update(updateSupplier);
            Context.SaveChanges();
        }
    }
}
