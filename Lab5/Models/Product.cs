﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab5.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Name Required") ]
        public string Name { get; set; }
        [Required(ErrorMessage = "Category Required")]
        public string Category { get; set; }
        [Required(ErrorMessage = "Price Required")]
        public decimal? Price { get; set; }
        [Required(ErrorMessage = "Select a Supplier plz")]
        public int? SupplierId { get; set; }
        public Supplier Supplier { get; set; }
    }
}
