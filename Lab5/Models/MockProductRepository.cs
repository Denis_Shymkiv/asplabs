﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab5.Models
{
    public class MockProductRepository : IRepositoryProduct
    {
        private readonly DbContextDatabase Context;

        public MockProductRepository(DbContextDatabase db) => Context = db;

        public IEnumerable<Product> GetAllProducts => Context.Products.Include(s=>s.Supplier);

        

        public void Create(Product NewProduct)
        {
            Context.Add(NewProduct);
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            Context.Products.Remove(new Product { Id = id });
            Context.SaveChanges();
        }

        public Product GetProduct(int id)
        {
            return Context.Products.Include(x => x.Supplier).First(x => x.Id==id);
        }

        public void Update(Product UpdateProduct)
        {
            Context.Products.Update(UpdateProduct);
            Context.SaveChanges();
        }

        public IEnumerable<Product> GetProductByIdSupplier(int id)
        {
            return Context.Products.Where(x=>x.SupplierId==id);

        }
        public string GetNameByIdSupplier (int id)
        {
            return Context.Suppliers.Where(x => x.Id == id).Select(x => x.Name).First();
        }

        




    }
}
