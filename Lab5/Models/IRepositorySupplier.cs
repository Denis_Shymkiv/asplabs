﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab5.Models
{
   public interface IRepositorySupplier
    {
        IEnumerable<Supplier> GetSupplier { get; }
        Supplier GetSupplierById(int id);
        void Create(Supplier newSupplier);
        void Update(Supplier updateSupplier);
        void Delete(int id);


    }
}
