﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Lab1.Models;
using System.Linq;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;

namespace Lab1.Controllers
{
  public class HomeController : Controller
    {
        List<TodoItem> items = new List<TodoItem> {
                new TodoItem { Id = 1, Name = "Sleep", IsComplete = true },
                new TodoItem { Id = 2, Name = "Eat", IsComplete = true },
                new TodoItem { Id = 3, Name = "Run", IsComplete = false },
                new TodoItem { Id = 4, Name = "Code", IsComplete = true },
                new TodoItem { Id = 5, Name = "Learn", IsComplete = false }
        };


        public IActionResult Index(string sortOrder)
        {
            List<TodoItem> list = new List<TodoItem>();
            switch (sortOrder)
            {
                case "done":
                    foreach (var item in items)
                        if (item.IsComplete)
                            list.Add(item);
                    return View(list.ToList());
                case "notdone":
                    foreach (var item in items)
                        if (!item.IsComplete)
                            list.Add(item);
                    return View(list.ToList());
                default:
                    return View(items.ToList());
            }
        }
    

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
