using System;
using System.Security.Claims;
using Lab4.App_Start;
using Lab4.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Lab4
{
  public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            services.AddDbContext<StoreDbContext>(config =>
            {
                config.UseInMemoryDatabase("db");
            });
            services.AddIdentity<IdentityUser, IdentityRole>(config=> {
                config.Password.RequireDigit = false;
                config.Password.RequiredLength = 1;
                config.Password.RequiredUniqueChars = 0;
                config.Password.RequireUppercase = false;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireLowercase = false;
            })
                .AddEntityFrameworkStores<StoreDbContext>()
                .AddDefaultTokenProviders();
            services.ConfigureApplicationCookie(config =>
            {
                config.LoginPath = "/Account/Login";
                config.Cookie.Name = CookieAuthenticationDefaults.AuthenticationScheme;
                config.ExpireTimeSpan = TimeSpan.FromDays(30);
            });
            
            services.AddAuthentication(config=> {

                config.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                config.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, config =>
                {
                    config.ExpireTimeSpan = TimeSpan.FromDays(30);
                    config.Cookie.Name = CookieAuthenticationDefaults.AuthenticationScheme;
                    config.LoginPath = "/Account/Login";
                    config.EventsType = typeof(CustomCookieAuthenticationEvents);
                });
            services.AddAuthorization(config=> {

                config.AddPolicy("claim.email", policyBuilder =>
                {
                    policyBuilder.RequireCustomClaim();
                });

                config.AddPolicy("role", policyBuilder =>
                {
                    policyBuilder.RequireClaim(ClaimTypes.Role);
                });

                config.AddPolicy("admin", policyBuilder =>
                {
                    policyBuilder.RequireClaim(ClaimTypes.Role,"admin");
                });

                config.AddPolicy("DoStuff", policyBuilder =>
                {
                    policyBuilder.RequireClaim("ToDo");
                });
            });
            services.AddSingleton<IAuthorizationPolicyProvider, CustomAuthorizationPolicyProvider>();
            services.AddScoped<IAuthorizationHandler, SecurityLevelHandler>();
            services.AddScoped<CustomCookieAuthenticationEvents>();
            services.AddScoped<IAuthorizationHandler, CustomRequireClaimHandler>();
            services.AddScoped<IAuthorizationHandler, CookieJarAuthizationHandler>();
            services.AddScoped<IClaimsTransformation, ClaimsTransformation>();
            _ = services.AddControllersWithViews(config=> {});
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
           
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
