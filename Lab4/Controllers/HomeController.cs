﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lab4.App_Start;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;


namespace Lab4.Controllers
{
  public class HomeController : Controller
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IOptions<AppSettings> _config;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(IAuthorizationService authorizationService,
            UserManager<IdentityUser> userManager,
            IOptions<AppSettings> config )
        {
            _authorizationService = authorizationService;
            _config = config;
            _userManager = userManager;
        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Policy = "role")]
        public IActionResult Admin()
        {
            return View();
        }
        [Authorize(Policy = "admin")]
        public async Task<IActionResult> Users()
        {
            var users = await this._userManager.Users.ToListAsync();
            return Ok(users);
        }
        public IActionResult AccessDenied() {
            return View();
        }
        public async Task<IActionResult> Open(string sortOrder)
        {
            var cookieJar = new CookieJar();
            cookieJar.Name = "Open";

            var result = await this._authorizationService.AuthorizeAsync(User, cookieJar, CookieJarAuthOperations.Open);
            if (result.Succeeded)
            {

                List<TodoItem> items = new List<TodoItem> {
                new TodoItem { Id = 1, Name = "Sleep", IsComplete = true },
                new TodoItem { Id = 2, Name = "Eat", IsComplete = true },
                new TodoItem { Id = 3, Name = "Run", IsComplete = false },
                new TodoItem { Id = 4, Name = "Code", IsComplete = true },
                new TodoItem { Id = 5, Name = "Learn", IsComplete = false }
            };
                List<TodoItem> list = new List<TodoItem>();
            switch (sortOrder)
            {
                case "done":
                    foreach (var item in items)
                        if (item.IsComplete)
                            list.Add(item);
                    return View(list.ToList());
                case "notdone":
                    foreach (var item in items)
                        if (!item.IsComplete)
                            list.Add(item);
                    return View(list.ToList());
                default:
                    return View(items.ToList());
            }
            }
            return RedirectToAction("AccessDenied");

        }
    }

    public class TodoItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}
