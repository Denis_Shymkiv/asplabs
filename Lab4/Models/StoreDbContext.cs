﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Lab4.Models
{
    public class StoreDbContext:IdentityDbContext
    {
        public StoreDbContext(DbContextOptions<StoreDbContext> options):base(options:options)
        {
        }
    }
}
