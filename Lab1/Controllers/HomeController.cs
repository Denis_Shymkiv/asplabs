﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Lab1.Models;
using System.Linq;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Lab1.Controllers
{
  public class HomeController : Controller
    {
        private readonly IWebHostEnvironment _env;
        public HomeController(IWebHostEnvironment env)
        {
            _env = env;
        }
        TodoItem[] items = new TodoItem[] {
                new TodoItem { Id = 1, Name = "Sleep", IsComplete = true },
                new TodoItem { Id = 2, Name = "Eat", IsComplete = false },
                new TodoItem { Id = 2, Name = "Learn", IsComplete = false }
        };

        public JsonResult Items()
        {
            return Json(items.ToList());
        }

        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Download()
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(Path.Combine(_env.ContentRootPath, "wwwroot/res/Views.pptx"));
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
            {
                throw new IOException("Cannot read file");
            }
            return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, "View.pptx");
        }
        
        
        public IActionResult Todo()
        {
            return View(items.ToList());
        }

        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
