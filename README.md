## Lab 1. MVC Beginning

Implement simple MVC-application. The application should containt one controller with at least three methods. The first method returns view where model data are displayed. The second one returns JSON-file. The third method download the file from the server.

[Open lab 1](https://aspnetlab1.herokuapp.com/)

---
## Lab 2. Views markup in ASP.NET MVC

* Demonstrate all variant of C# code usage in code of the page (.cshtml).
* Generate a list of items using for loop.
* Show computation of values in the embedded block of code.
* Show conditional generation of markup using if operator.

[Open lab 2](https://aspnetlab2.herokuapp.com/)

---
## Lab 3. Forms in ASP.NET MVC

Implement processing of the data obtained from the user using form components:

1. Text box.
2. Check box.
3. Radio box.
4. Drop-down list.
5. Submit button.

Use helpers for generation of the abovementioned components. Demonstrate both POST and GET requests.

[Open lab 3](https://aspnetlab3.herokuapp.com/)

---
## Lab 4. Security configuration in ASP.NET MVC

Demonstrate knowledge of the main ASP.NET security components by:

1. Creating project with authentication feature (standard option, local DB).
2. Limiting access to the part of the application using specialized attributes (AuthorizedAttribute).
3. Accessing information about the current user and displaying it in the custom view.

[Open lab 4](https://aspnetlab4.herokuapp.com/)

---
## Lab 5. Accessing data in ASP.NET MVC

Demonstrate possiblities of Entity Framework for ASP.NET MVC (Code-First approach) by generating 3 tables, populating them with some data, and providing necessary components to display and edit entries using web-interface, add and delete entries. Scaffolding option is recommended for this work.

[Open lab 5](https://aspnetlab5.herokuapp.com/)

---
## Маршрутизація у ASP.NET MVC

Використовувати у web-застосунку 3 власні шаблони для URL, зареєстровані у RegisterRoutes. Продемонструвати генерацію посилань за допомогою ActionLink для усіх 3 шаблонів, а також коректність переходу на відповідні ресурси. Продемонструвати генерацію посилань за допомогою RouteLink для усіх 3 шаблонів, а також коректність переходу на відповідні ресурси. Продемонструвати передачу параметрів у метод контролеру за допомогою URL, а також продемонструвати, що переданий параметр(-и) дійсно використовується під час роботи контролеру. У якості параметру передавати:

1. Рядок.
2. Числове значення.
3. Декілька (щонайменше 2 параметри) різних типів.

Продемонструвати використання перенаправлення: перенаправити на виконання методу іншого (не поточного контролеру). Продемонструвати використання зон у web-застосунку: необхідно створити щонайменше 2 зони.

[Open lab](https://aspnetlab.herokuapp.com/)

## AJAX у ASP.NET MVC

Реалізувати власний клас моделі та реалізувати завантаження інформації асинхронним запитом про екземпляр цього класу за допомогою посилання. Інформація має відображатись на тій самій сторінці, звідки запит ініційований.
Реалізувати пошук за допомогою форми по колекції об'єктів (не менше 10) по 3 полям. Запит має виконуватись асинхронно, а виведення результату відбувається на ту саму сторінку без її перезавантаження.

[Open lab](https://aspnetlab.herokuapp.com/)

## Самостійна робота з дисципліни

У якості самостійної роботи студентам пропонується реалізувати власний Proof-of-Concept проект з використанням технології ASP.NET MVC. Предметна область обирається студентом та узгоджується з керівником курсу. Кожен самостійний проект має містити:

* реалізацію роботи з базою даних (на основі Code-First або Data-First підходів);
* відображення даних з бази даних;
* додавання та видалення даних безпосередньо з сайту;
* пошук за параметрами;
* реалізацію авторизації та аутентифікації.

Приклади тем для виконання самостійної роботи:

* автомобілі;
* продуктові товари;
* книги;
* принтери;
* процесори;
* меблі;
* облік послуг.

Реалізація кожного пункту оцінюється у 2 бали. При оцінюванні враховується коректність роботи функцій, повнота реалізації, відповідність реалізованого рішення найкращим практикам по роботі з даним та реалізації MVC-застосунку.

[Open lab](https://aspnetlab.herokuapp.com/)

## [Reports](https://www.dropbox.com/sh/fdo4e16djp4mw42/AABfcgc060kcF2qNv0hkvx9na?dl=0)